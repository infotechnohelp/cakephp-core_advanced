# infotechnohelp/cakephp-core_advanced

## Structure

### src/Controller/AppController extending structure

`Core ← DefaultSkeleton ← Skeleton ← App ← Core\Api ← DefaultSkeleton\Api ← Skeleton\Api ← App\Api`

### src/Template/Layout/default.ctp extending structure

`Core ← DefaultSkeleton ← Skeleton ← App`

## Implementation

### 1. Add plugin

##### src/Application.php → bootstrap()

`$this->addPlugin('Infotechnohelp/Core', ['bootstrap' => true, routes' => true]);`

---

### Routes

`config/routes.php`

```
Router::defaultRouteClass(DashedRoute::class);

Configure::write('Infotechnohelp.Routes.Connected', ['app' => function (RouteBuilder $routes) {

    $routes->connect('/', ['controller' => 'Home', 'action' => 'index', 'home']);
    ...

}]);
```

### CSRF

#### Implement HTML form

`src/Template/*/action.ctp`

##### CakePHP FormHelper

```
echo $this->Form->create().
$this->Form->control('name').
$this->Form->submit('Submit', ['class' => 'class1 class2']).
$this->Form->end();    
```

##### Plain HTML form

`src/Controller/*Controller.php`

```
public function action(){
    $this->set('csrfToken', $this->getRequest()->getParam('_csrfToken'));
}
```

`src/Template/*/action.ctp`

```
<form action="" method="post">
    <input type="hidden" name="_csrfToken" autocomplete="off" value="<?= $csrfToken ?>"/>
    <input name="name">
    <button type="submit">Submit</button>
</form>
```

#### Implement in AJAX call

`src/Template/Layout/default.ctp` or any other `.ctp`

##### jQuery

POST data param `_csrfToken`

```
var csrfToken = <?= $this->request->getParam('_csrfToken'); ?>;

$.ajax({
         method: 'POST',
         data: {
            _csrfToken: csrfToken 
         },
         ...
});
```

Header

```
var csrfToken = <?= $this->request->getParam('_csrfToken'); ?>;

$.ajax({
         beforeSend: function (xhr) {
             xhr.setRequestHeader('X-CSRF-Token', csrfToken);
         },
         ...
});
```



### JsonApi implementation

#### Add config

`Api/...Controller.php`

Request handler component should be loaded

```
$this->loadComponent('RequestHandler');
```

`config/infotechnohelp.json-api.yml`

``` 
pathPrefixes:
- api
- auth-api (automatically later)
```

#### Set ErrorHandlerMiddleware (JSON response structure)

Replace manually in `src/Application.php`

`use Cake\Error\Middleware\ErrorHandlerMiddleware;` → `use Infotechnohelp\JsonApi\Middleware\ErrorHandlerMiddleware;`

---

### Authentication implementation

