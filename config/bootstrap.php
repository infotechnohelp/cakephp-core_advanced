<?php

/** @var \App\Application $Application */
$Application = new \App\Application(CONFIG);

$Application->addPlugin('Infotechnohelp/JsonApi', ['routes' => true]);

$Application->addPlugin('Infotechnohelp/Authentication', ['routes' => true]);