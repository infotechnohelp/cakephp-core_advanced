<?php

declare(strict_types=1);

namespace Infotechnohelp\Core\Controller;

use \Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Languages\Lib\LocaleManager;

/**
 * Class AppController
 * @package Core\Controller
 */
class AppController extends Controller
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Cookie');

        $this->set('Cookie', $this->Cookie);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Infotechnohelp/Authentication.Authentication');
    }
}
