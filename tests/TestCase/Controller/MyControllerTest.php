<?php

namespace Infotechnohelp\Core\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\InvalidCsrfTokenException;
use Cake\TestSuite\IntegrationTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MyControllerTest
 * @package Infotechnohelp\Core\Test\TestCase\Controller
 */
class MyControllerTest extends IntegrationTestCase
{
    public function testConnectedRoute()
    {
        $this->session(['Auth' => ['User' => ['id' => 1]]]);

        $this->get('my/hello-world');
        $this->assertResponseOk();
        $responseStringBody = $this->_getBodyAsString();
        $this->assertEquals('Hello world', $responseStringBody);


        $this->get('/');
        $this->assertResponseOk();
        $this->assertEquals($responseStringBody, $this->_getBodyAsString());
    }

    public function testCsrf()
    {
        $this->session(['Auth' => ['User' => ['id' => 1]]]);

        $token = 'my-csrf-token';

        $this->cookie('csrfToken', $token);

        $this->post('my/hello-world', [
            '_csrfToken' => $token
        ]);

        $this->assertResponseOk();
    }

    public function testCsrfConnectedAppRoute()
    {
        $this->session(['Auth' => ['User' => ['id' => 1]]]);

        $token = 'my-csrf-token';

        $this->cookie('csrfToken', $token);

        $this->post('/', [
            '_csrfToken' => $token
        ]);

        $this->assertResponseOk();
    }

    public function testCsrfException()
    {
        $this->session(['Auth' => ['User' => ['id' => 1]]]);

        $this->post('my/hello-world');

        $this->assertResponseCode(500);

    }
}
