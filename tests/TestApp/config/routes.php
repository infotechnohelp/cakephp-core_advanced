<?php

declare(strict_types=1);

use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use \Cake\Routing\RouteBuilder;
use Cake\Core\Configure;

Router::defaultRouteClass(DashedRoute::class);

Configure::write('Infotechnohelp.Routes.Connected', ['app' => function (RouteBuilder $routes) {

    $routes->connect('/', ['controller' => 'My', 'action' => 'hello-world', 'home']);

}]);