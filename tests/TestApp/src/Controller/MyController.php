<?php

namespace TestApp\Controller;

class MyController extends AppController
{

    public function helloWorld()
    {
        $this->autoRender = false;
        $this->setResponse($this->getResponse()->withStringBody('Hello world'));
    }
}