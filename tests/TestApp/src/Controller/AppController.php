<?php

namespace TestApp\Controller;


class AppController extends \Infotechnohelp\Core\Controller\AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
    }
}